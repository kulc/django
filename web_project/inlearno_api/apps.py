from django.apps import AppConfig


class InlearnoApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'inlearno_api'
