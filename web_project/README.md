# Install environments

```
py -3 -m venv .venv
```

# Install and run

```
cd ./web_project
```

## Install app

```
pip install -r requirements.txt
```

## Init DB

```
python manage.py migrate
```

## Start

```
python manage.py runserver localhost:8000
```